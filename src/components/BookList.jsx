import React, { Component } from "react";
import Book from "./Book";
import "../App.css";

export default class BookList extends Component {
  constructor() {
    super();
    this.state = {
      book: [
        {
          id: 1,
          title: "មនុស្សពីរនាក់នៅផ្ទះជិតគ្នា",
          publishedYear: 2012,
          isHiding: false,
        },
        {
          id: 2,
          title: "គង់ហ៊ាន",
          publishedYear: 2015,
          isHiding: false,
        },
        {
          id: 3,
          title: "មនុស្សចេះថ្នាំពិសេស",
          publishedYear: 2018,
          isHiding: false,
        },
        {
          id: 4,
          title: "អណ្តើកនិងស្វា",
          publishedYear: 2019,
          isHiding: false,
        },
      ],
    };
  }

  acton(){
      
  }

  render() {
    let myBook = this.state.book.map((book) => (
      <tr key={book.id}>
        <td>{book.id}</td>
        <td>{book.name}</td>
        <td>{book.publishedYear}</td>
        <td>{book.isHiding}</td>
        <td><input type="button" value="Show" onClick={this.acton.bind(this)}/></td>
      </tr>
    ));

    return <div>
        <Book myBook={myBook}/>
    </div>;
  }
}
