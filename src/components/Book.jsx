import React from 'react'

export default function Book(props) {
    return (
        <div>
            <h1>Book List</h1>
            <table>
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Title</td>
                        <td>Published Year</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    {props.myBook}
                </tbody>
            </table>
        </div>
    )
}
