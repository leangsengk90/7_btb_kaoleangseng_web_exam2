import React from 'react';

import BookList from './components/BookList';

function App() {
  return (
    <div className="App">
        <h1>Kao Leangseng</h1>
        <BookList />
    </div>
  );
}

export default App;
